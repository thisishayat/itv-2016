<!--header-->   
  <?php get_header(); ?>
<!--header end-->
		
		<!--main-video-->
		<div id="main-video">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
						
						<?php if(have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>    
						 <?php the_content(); ?>
						<?php endwhile; ?>
						<?php else : ?>
                                       <article class="thumbnail blog-post-total">
                      
                                          <div class="alert alert-error blog-post-total-error">
                                            404 Error. <br /> Nothig Found. <br /> Please Try Again.
                                          </div>
                                          
                                      </article>
                          
						<?php endif; ?>
						
					</div>
				</div>
			</div>
		</div>
		<!-- //main-video-->
		
		
		
		
		
		
		
		
		
		
		
<!--footer-->          
<?php get_footer(); ?>
<!--footer end-->