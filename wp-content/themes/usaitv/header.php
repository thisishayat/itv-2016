<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        <?php
        if (is_front_page()) {
            bloginfo('name'); echo ' | '; bloginfo('description'); }
        else{
            if (function_exists('is_tag') && is_tag()) {
                single_tag_title("Tag Archive for &quot;"); echo '&quot; | '; }
            elseif (is_archive()) {
                wp_title(''); echo ' Archive - '; }
            elseif (is_search()) {
                echo 'Search for &quot;'.wp_specialchars($s).'&quot; | '; }
            elseif (!(is_404()) && (is_single()) || (is_page())) {
                wp_title(''); echo ' | '; bloginfo('name'); echo ' | '; bloginfo('description'); }
            elseif (is_404()) {
                echo 'Not Found | '; }


            if ($paged>1) {
                echo ' | page '. $paged; }
        }
        ?>
    </title>

    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/css/responsive.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body style='background-image: url("http://itvusa.tv/wp-content/uploads/2016/08/chk_captcha.jpeg");'>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=242787812436050";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!--navigation-->
<div class="col-md-12" style="margin-bottom: 10px">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 header-navigation-menu" style="padding-top: 70px;">

            </div>
        </div>
    </div>
</div>

<!--//navigation-->