<!-- footer-area -->
<div id="footer-area-bottom" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <p> Copyright &copy Muhammad Shahidullah</p>
                </div>
                <div class="col-md-6" id="social_icon">
                    <ul>
                        <li><a href="https://www.facebook.com/itvusa.tv/"><img src="<?php bloginfo('template_url'); ?>/images/sm-icons/facebook_32.png"></a></li>
                        <li><a href="https://plus.google.com/u/0/102562715989356599590"><img src="<?php bloginfo('template_url'); ?>/images/sm-icons/googleplus_32.png"></a></li>
                        <li><a href="https://twitter.com/Itvusadottv"><img src="<?php bloginfo('template_url'); ?>/images/sm-icons/twitter_32.png"></a></li>
                        <li><a href="https://www.youtube.com/channel/UCGtTY378_sSHy8WejVp1vvg"><img src="<?php bloginfo('template_url'); ?>/images/sm-icons/youtube_32.png"></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- //footer-area -->


<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
